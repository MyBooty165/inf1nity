// Code for handling the search form submission
document.querySelector("form").addEventListener("submit", (event) => {
  event.preventDefault();
  const searchInput = document.querySelector("input[type=text]");
  window.location = `https://www.google.com/search?q=${searchInput.value}`;
});
